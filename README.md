Some day ...  
**Description:**  
Responsible for running of invoice_ projects environment 
using docker-compose with exposed volumes for easy development.  

**One command run after cloning only this repo:**  
bash run.sh    

**Services**:  
[http://localhost:3000](http://localhost:3000) - 
frontend nuxt application  
[http://localhost:8000/graphql](http://localhost:8000/graphql) - 
django application  
[http://localhost:8888](http://localhost:8888) -
java endpoint for generating invoices  

**Desired directory structure:**  
invoice_runner/  
  - invoice_backend/  
  - invoice_frontend/  
  - invoice_runner/

**Run:**   
`docker-compose up` 

**Troubleshoting**
1. **locahost services are not responding:**  
probably you have docker-machine so run 
`docker-machine ip` and then instead of localhost
type ip you got 