#!/usr/bin/env bash
if [ ! -L /invoice_pdf/fonts ];
then
	ln -s /data/fonts /invoice_pdf/fonts
fi
(cd /invoice_pdf;mvn install)
(cd /invoice_pdf;mvn exec:java)